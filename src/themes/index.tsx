import Fonts from './fonts';
import Colors from './colors';
import Metrics from './metrics';
import Images from './images';

const FontType = {
    extraBold: 'Lato-Black',
    extraBoldItalic: 'Lato-BlackItalic',
    bold: 'Lato-Bold',
    boldItalic: 'Lato-BoldItalic',
    regular: 'Lato-Regular',
    light: 'Lato-Light',
    thin: 'Lato-Thin',
};
export {Colors, Metrics, Fonts, Images};