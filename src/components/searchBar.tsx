import React, { useLayoutEffect , useState} from 'react';
import {
    Dimensions,
    Image,
    StatusBar,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    View,
    Text,
    Platform,
    UIManager,
    LayoutAnimation
} from 'react-native';


import {Colors, Images} from 'themes';
var {width} = Dimensions.get('window');

export default function SearchBar(props: any) {
    const [show, setShow] = useState(false);
    // const navigation = useNavigation();

    if (Platform.OS === "android") {
        if (UIManager.setLayoutAnimationEnabledExperimental) {
          UIManager.setLayoutAnimationEnabledExperimental(true);
        }
      }

    useLayoutEffect(()=> {
        props.navigation.setOptions({
            headerRight: right, 
            headerTitle: show ? '' :'Search Movie',
        })
    },[show, props])

    function showHeader(){
        setShow(!show);
        show && props.searched('');
        LayoutAnimation.configureNext(
            LayoutAnimation.create(
              150,
              LayoutAnimation.Types.linear,
              LayoutAnimation.Properties.scaleX
            )
        );
    }

    return null;
    function right () {
    return(
        <View style = {styles.container}>
            <TouchableOpacity style={styles.iconView} onPress={showHeader}>
            <Image source={show ?  Images.clear : Images.search} style={styles.icon} resizeMode={'contain'} />
            </TouchableOpacity>
               {show ? <>
                <TextInput
                    style = {styles.textInputStyle}
                    placeholder={'Search here...'}
                    onChangeText={(text: string) => props.searched(text)}
                    value={props.value}
                    autoFocus={true}
                    underlineColorAndroid={'transparent'}
                    onSubmitEditing={props.done} />
                    <TouchableOpacity style={styles.iconView} onPress={()=> props.done()}>
                        <Text style={styles.goTxt}>Go</Text>
                    </TouchableOpacity>
                </> : <></>}
        </View>
        )
    }
};

const styles = StyleSheet.create({
    iconView: {
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
    },
    container: {
        flex:1,
        flexDirection: 'row',
    },
    textInputStyle: {
        fontSize: 16,
        width: width*0.79,
    },
    icon: {
        height:25,
        width:25,
        tintColor: Colors.themeColor,
    },
    goTxt: {
        color: Colors.themeColor,
        fontWeight: 'bold',
        paddingRight: 5,
    }
});