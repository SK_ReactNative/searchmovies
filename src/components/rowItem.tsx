import { func } from 'prop-types';
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import { Metrics } from 'themes';

export default function RowItem({title, value}: any){
    return (
    <View style={s.contain}>
        <Text style={s.title}>{title}: </Text>
        <Text style={s.value}>{value}</Text>
    </View>
    )
};

const s = StyleSheet.create({
   contain: {
       flexDirection: 'row',
    //    width: '64.5%',
       justifyContent: 'center',
   },
   title: {
       fontSize: Metrics.rfv(12),
       fontWeight: 'bold',
   },
   value: {
    fontSize: Metrics.rfv(12),
   }
})