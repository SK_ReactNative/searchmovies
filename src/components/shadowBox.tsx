import React from "react";
import {Platform, View, TouchableOpacity} from "react-native";

const style = {
  ...Platform.select({
    ios: {
      shadowColor: '#000',
      shadowOffset: {width: 1, height: 2},
      shadowOpacity: 0.41,
      shadowRadius: 4.46,
    },
    android: {
      elevation: 2,
    },
  }),
  margin: 10,
  borderRadius: 10,
  backgroundColor: "white",
  padding: 10,
  flex: 1,
};

export default function Shadow(props: any) {
  return (
    <TouchableOpacity
      disabled={!props.onPress || props.disabled}
      onPress={props.onPress}
      onLongPress={props.onLongPress}
      activeOpacity={props.activeOpacity || 0.7}>
        <View needsOffscreenAlphaCompositing={true} style={[style, props.style]}>
        {props.children}
        </View>
    </TouchableOpacity>
  );
}
