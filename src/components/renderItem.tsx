import React from 'react';
import {View, Text, Image} from 'react-native';
import FastImage from 'react-native-fast-image';
import { RootStateOrAny, useSelector } from "react-redux";
import { Images} from '../themes';
import RowItem from './rowItem';
import Shadow from './shadowBox';
import styles from './styles';

export default function RenderItem(props: any){
    return (
        <Shadow onPress={props.onPress} style={styles.movieCard}>
            {props.saved && <Image style={styles.saved} source={Images.saved}/>}
        <FastImage
            style={styles.posterIcon}
            resizeMode={FastImage.resizeMode.stretch}
            source={props.Poster != 'N/A' ? {
                uri: props.Poster,
                priority: FastImage.priority.high,
            }: Images.poster}/>
        <View style={styles.titleBox}>
            <Text style={styles.title}>{props.Title}</Text>
            <RowItem title={'Released On'} value={ props.Year} />
            {/* <Text style={styles.title}>{props.year}</Text> */}
        </View>
        </Shadow>
    )
}