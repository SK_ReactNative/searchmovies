import React from "react";
import { ActivityIndicator, Modal, View } from "react-native";
import styles from "./styles";
import { RootStateOrAny, useSelector } from 'react-redux';
import { Colors } from "themes";


export default function Loader() {
  const {loading} = useSelector((state: RootStateOrAny) => state.list);
  if (!loading) {
    return null;
  }

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={loading}>
      <View style={styles.modal}>
        <View style={styles.box}>
        <ActivityIndicator size={60} color={Colors.themeColor} />
        </View>
      </View>
    </Modal>
  );
}
