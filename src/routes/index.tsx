import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  createStackNavigator,
  // useHeaderHeight
} from "@react-navigation/stack";
import Movies from '../containers/movies';
import ShortlistMovies from '../containers/shortlistMovies';
import { Alert, Image, Pressable, StyleSheet, Text } from 'react-native';
import { Images, Metrics, Colors } from 'themes';

const Tab = createBottomTabNavigator();

const Stack = createStackNavigator();
function PageOne() {
  return (
    <Stack.Navigator>
      <Stack.Screen options={{
        headerTitleAlign:'center',
        title:'Search Movies here',
        headerTitleStyle:{color: Colors.themeColor}
      }} name="Movies" component={Movies} />
    </Stack.Navigator>
  );
}
function PageTwo() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{
          headerTitleAlign: "center",
          headerTitleStyle: { color: Colors.themeColor },
        }}
        name="shortlisted Movies"
        component={ShortlistMovies}
      />
    </Stack.Navigator>
  );
}

function MyTabs() {
  return (
    <Tab.Navigator tabBarOptions={{
      keyboardHidesTabBar:true,
      activeBackgroundColor: Colors.bg,
      activeTintColor: Colors.themeColor,
    }}>
      <Tab.Screen options={{
        tabBarIcon:(props)=> <Image source={Images.movie} style={[s.icon, {backgroundColor: props.color}]} />, //{ focused: boolean, color: string, size: number }
      }} name="Page 1" component={PageOne} />
      <Tab.Screen name="Page 2"
      options={{
        tabBarIcon: (props) => <Image source={Images.car} style={[s.icon,{backgroundColor: props.color}]} />, //{ focused: boolean, color: string, size: number }
      }} component={PageTwo} />
    </Tab.Navigator>
  );
}
const s = StyleSheet.create({
  icon: {
    height: Metrics.rfv(36),
    marginTop: 4,
    width: Metrics.rfv(36),
    borderRadius: 20,
    padding: 15,
    tintColor: Colors.white
  },

})

export default MyTabs;