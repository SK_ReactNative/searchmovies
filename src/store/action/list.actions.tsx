import instance from 'utils/api';
import {API_PATH, BASE_URL, TYPE} from 'utils/strings';
import {print} from 'utils/function';
import axios from 'axios';
import _ from 'lodash';

interface IActions {
    CAR_LIST :string,
    MOVIE_LIST: string,
  } 

interface Dispatch {
    type: IActions['MOVIE_LIST'];
    loading: boolean;
}

export const loading = (payload: Boolean) => ({type: TYPE.load, payload});
export const setMovieList = (payload: any) => ({type: TYPE.movieList, payload});
export const setTotalPage = (payload: number) => ({type: TYPE.movieTPage, payload})

export const getMovieList =(params: any) => async (dispatch: any, getState: any) => {
    try {
        dispatch(loading(true));
        const {movieList} = getState().list;
        params['apikey'] = 'a1b5f9ec';
        const res = await axios.get(BASE_URL, {params});
        let total = res.data.totalResults/10;
        dispatch(loading(false));
        dispatch(setTotalPage(total));
        print(res.data);
        let data = params.page != 1 ? movieList.concat(res.data.Search): res.data.Search;
        dispatch(setMovieList(data));
    } catch (error) {
        dispatch(loading(false));
        throw error;
    }
}

function removeKey(obj: any, key: string){
    delete obj[key];
    return obj
}

export const setSortlisted =(data: any) => async (dispatch: any, getState: any) => {
    try {
        let {sortList} = getState().list;
        let newSortList = _.cloneDeep(sortList)
        let payload = (newSortList && data.imdbID in newSortList) ? removeKey(newSortList, data.imdbID) : {...newSortList, [data.imdbID] : data};
        dispatch({type: TYPE.sortList, payload: payload});
    } catch (error) {
    }
}