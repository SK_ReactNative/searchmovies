import { createStore, applyMiddleware, combineReducers } from 'redux';
import { persistStore, persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";
import thunk from 'redux-thunk';
import ListReducer from './reducer/list.reducer';

const rootPersistConfig: any = {
  key: "root",
  storage: AsyncStorage,
  blacklist: ["list"],
};

const listPersistConfig: any = {
  key: "list",
  storage: AsyncStorage,
  whitelist: ["sortList"],
};

const rootReducer = combineReducers({
  list: persistReducer(listPersistConfig, ListReducer),
});

const persistedReducer = persistReducer(rootPersistConfig, rootReducer);
const store = createStore(persistedReducer, applyMiddleware(thunk));
let persistor = persistStore(store);
export { store, persistor };