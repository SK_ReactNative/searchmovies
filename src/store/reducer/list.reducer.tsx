import { TYPE } from "utils/strings";

const initialState = {
    loading: false,
    sortList: {},
    movieList: [],
    movieTpage: 1,
};

const ListReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case TYPE.load:
            return {...state, loading: action.payload};
        case TYPE.movieList:
            return { ...state, movieList: action.payload }; //feedback: action.payload
        case TYPE.movieTPage:
            return { ...state, movieTpage : action.payload };
        case TYPE.sortList:
            return {...state, sortList: action.payload};
         default:
            return state;
    }
}

export default ListReducer;