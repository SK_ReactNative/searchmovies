import React, { useEffect } from 'react';
import Empty from 'components/emptyScr';
import MovieCard from 'components/renderItem';
import styles from './styles';
import {FlatList, View} from 'react-native';
import { RootStateOrAny, useSelector } from 'react-redux';

function ShortlistMovies (props: any) {
    const {sortList} = useSelector((state: RootStateOrAny) => state.list);
    return (
        <FlatList
            numColumns={2}
            extraData={props}
            style={styles.container}
            data={sortList && Object.values(sortList)}
            renderItem={({item, index}) =>  <MovieCard key={index} {...item}/>}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={()=> <Empty/>}
        />
    )
}

export default ShortlistMovies;