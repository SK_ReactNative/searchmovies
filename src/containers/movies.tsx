import Empty from 'components/emptyScr';
import RenderItem from 'components/renderItem';
import React, { useEffect, useState } from 'react';
import {FlatList, Text, View, TouchableOpacity} from 'react-native';
import {RootStateOrAny,  useDispatch, useSelector } from 'react-redux';
import { getMovieList, setSortlisted } from 'store/action/list.actions';
import { print } from 'utils/function';
import Search from 'components/searchBar';
import { MOVIE_TYPE } from 'utils/strings';
import styles from './styles';
// import { Colors } from 'themes';
import { useNavigation } from '@react-navigation/native';

function MoviesList (props: any) {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const {movieList, movieTpage, sortList} = useSelector((state: RootStateOrAny) => state.list);
    const [page, setPage] = useState(1);
    const [type, setType] = useState<String>('movie');
    const [search, setSearch] = useState<String>('naruto');
    const [done, setDone] = useState<Boolean>(false);

    useEffect(()=> {
        fetch();
    },[]);

    async function fetch(tt?: string){
        try {
        let params = {type, s: search, page};
        if(tt) {
            setType(tt);
            params['type'] = tt;
        }
       await dispatch(getMovieList(params));
       setDone(true);
       print(movieList);
        } catch (error) {
        setDone(true)
        }
    }

    async function loadMore(){
        let pg = page+1;
        let params = {type, s: search, page: pg};
        setPage(pg);
       await dispatch(getMovieList(params));
    };

    function MovieType(){
       return <View style={styles.typeView}>
        {MOVIE_TYPE.map((item: string, index: number)=> (
            <TouchableOpacity onPress={()=> fetch(item)} style={[styles.typebtn, item== type && {backgroundColor: 'gray'}]} key={index}>
                <Text style={[styles.typeTxt, type == item && {color: "white"}]}>{item}</Text>
            </TouchableOpacity>
        ))}
        </View>
    }

    function setSorted(item: object){
        dispatch(setSortlisted(item));
    }

    return (
            <View style={styles.container}>
            <Search 
                searched={(val: string)=> setSearch(val)}
                value={search}
                done={()=> fetch()}
                navigation={navigation}
            />
            {/* {MovieType()} */}
            <FlatList
                numColumns={2}
                data={movieList}
                extraData={props}
                showsVerticalScrollIndicator={false}
                ListFooterComponent={(movieTpage == page || !movieList?.length) ? null : <Text onPress={loadMore} style={styles.loadMore}>Load More...</Text>}
                renderItem={({item, index}) =>  <RenderItem 
                onPress={()=> setSorted(item)}
                saved={item.imdbID in sortList}
                key={index} {...item}/>}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={()=> done && <Empty />}
            />
            </View>
    )
}

export default MoviesList;