import axios from 'axios';
import {BASE_URL} from 'utils/strings';

// axios.defaults.baseURL = BASE_URL;
// axios.defaults.timeout = 45000;

const instance = axios.create({
    baseURL:  BASE_URL,
    timeout: 30 * 1000,
  });
export default instance;

// export async function post(path: string, params: any){
//     try {
//         const res = await axios.post(path, params);
//         return res;
//     } catch (error) {
        
//     }
// }